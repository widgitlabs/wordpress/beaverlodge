<?php
/**
 * Plugin Name:     BeaverLodge
 * Plugin URI:      https://widgitlabs.com
 * Description:     Open-source Beaver Builder additions
 * Author:          Widgit Team
 * Author URI:      https://widgitlabs.com
 * Version:         1.0.0
 * Text Domain:     beaverlodge
 * Domain Path:     languages
 *
 * @package         Widgit\BeaverLodge
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'BeaverLodge' ) ) {

	/**
	 * Main BeaverLodge class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class BeaverLodge {


		/**
		 * The one true BeaverLodge
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         BeaverLodge $instance The one true BeaverLodge.
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object.
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true BeaverLodge.
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof BeaverLodge ) ) {
				self::$instance = new BeaverLodge();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'beaverlodge' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'beaverlodge' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'BEAVERLODGE_VER' ) ) {
				define( 'BEAVERLODGE_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'BEAVERLODGE_DIR' ) ) {
				define( 'BEAVERLODGE_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'BEAVERLODGE_URL' ) ) {
				define( 'BEAVERLODGE_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'BEAVERLODGE_FILE' ) ) {
				define( 'BEAVERLODGE_FILE', __FILE__ );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			if ( class_exists( 'FLBuilder' ) ) {
				WP_Filesystem();

				global $wp_filesystem;

				$modules = $wp_filesystem->dirlist( BEAVERLODGE_DIR . 'modules', false );

				foreach ( $modules as $module_id => $module_data ) {
					$module_file   = BEAVERLODGE_DIR . 'modules/' . $module_id . '/' . $module_id . '.php';
					$module_loader = BEAVERLODGE_DIR . 'modules/' . $module_id . '/register.php';
					
					if ( $wp_filesystem->exists( $module_file ) && $wp_filesystem->exists( $module_loader ) ) {
						require_once $module_file;
						require_once $module_loader;
					}
				}
			}
			
			require_once BEAVERLODGE_DIR . 'includes/helper-functions.php';
			require_once BEAVERLODGE_DIR . 'includes/scripts.php';

			// if ( is_admin() ) {
			// 	require_once BEAVERLODGE_DIR . 'includes/admin/meta-boxes.php';
			// }
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'beaverlodge_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'beaverlodge' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'beaverlodge', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/beaverlodge/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/beaverlodge/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/beaverlodge folder.
				load_textdomain( 'beaverlodge', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/beaverlodge/languages/ folder.
				load_textdomain( 'beaverlodge', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/beaverlodge/ folder.
				load_textdomain( 'beaverlodge', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'beaverlodge', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true BeaverLodge
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $beaverlodge = BeaverLodge(); ?>
 *
 * @since       1.0.0
 * @return      BeaverLodge The one true BeaverLodge
 */
function beaverlodge() {
	return BeaverLodge::instance();
}

// Get things started.
beaverlodge();
