(function($) {
    BeaverLodge_Event_Showcase = function( settings ) {
        this.id        = settings.id;
        this.nodeClass = '.fl-node-' + settings.id;
        this.showcase  = $( this.nodeClass ).find( '.beaverlodge-event-showcase' ).first();
        this.perView   = settings.perView;

        this._init();
    };

    BeaverLodge_Event_Showcase.prototype = {
        nodeClass: '',
        perView: '',

        _init: function() {
            var self = this;
            new Swiper( self.showcase.get(), {
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
                slidesPerView: self.perView
            });
        }
    };
})(jQuery);