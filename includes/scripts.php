<?php
/**
 * Scripts
 *
 * @package     BeaverLodge\Scripts
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Load scripts
 *
 * @since       1.0.0
 * @return      void
 */
function beaverlodge_enqueue_scripts() {
	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	// Setup versioning for internal assets.
	// $css_ver = gmdate( 'ymd-Gis', filemtime( BEAVERLODGE_DIR . 'assets/css/beaverlodge' . $suffix . '.css' ) );
	// $js_ver  = gmdate( 'ymd-Gis', filemtime( BEAVERLODGE_DIR . 'assets/js/beaverlodge' . $suffix . '.js' ) );

    wp_register_style( 'beaverlodge-swiper', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), '8' );
    wp_enqueue_style( 'beaverlodge-swiper' );

    wp_enqueue_script( 'beaverlodge-swiper', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array( 'jquery' ), '8', true );

	// wp_register_style( 'beaverlodge', BEAVERLODGE_URL . 'assets/css/beaverlodge' . $suffix . '.css', array(), BEAVERLODGE_VER . '-' . $css_ver );
	// wp_enqueue_style( 'beaverlodge' );

	// wp_enqueue_script( 'beaverlodge', BEAVERLODGE_URL . 'assets/js/beaverlodge' . $suffix . '.js', array( 'jquery' ), BEAVERLODGE_VER . '-' . $js_ver, true );
}
add_action( 'wp_enqueue_scripts', 'beaverlodge_enqueue_scripts' );
