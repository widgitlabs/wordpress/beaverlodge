<?php
/**
 * Event Browser module
 *
 * @package     BeaverLodge\Modules\Event_Browser
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Event Browser module class
 *
 * @since       1.0.0
 */
class BeaverLodge_Event_Browser extends FLBuilderModule {


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function __construct() {
		parent::__construct(
			array(
				'name'            => __( 'Event Browser', 'beaverlodge' ),
				'description'     => __( 'Browse your Event Calendar events', 'beaverlodge' ),
				'group'           => __( 'BeaverLodge Kits', 'beaverlodge' ),
				'category'        => __( 'Event Calendar Modules', 'beaverlodge' ),
				'dir'             => BEAVERLODGE_DIR . 'modules/beaverlodge-event-browser/',
				'url'             => BEAVERLODGE_URL . 'modules/beaverlodge-event-browser/',
				'icon'            => 'icon.svg',
				'editor_export'   => true,
				'enabled'         => true,
				'partial_refresh' => true,
	  		)
		);
	}
}
