<?php
/**
 * Bootstrap our test suites
 *
 * @package     Widgit\Mod_Manager\Tests\Bootstrap
 * @since       2.0.0
 */

$_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
$_SERVER['SERVER_NAME']     = '';
$_SERVER['PHP_SELF']        = '/index.php';
$GLOBALS['PHP_SELF']        = isset( $_SERVER['PHP_SELF'] ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride
$PHP_SELF                   = $GLOBALS['PHP_SELF'];          // phpcs:ignore WordPress.WP.GlobalVariablesOverride

define( 'WP_USE_THEMES', false );

/**
 * Support for the PHPUnit Polyfills library
 */
$_polyfills_path = getenv( 'WP_TESTS_PHPUNIT_POLYFILLS_PATH' );
if ( ! defined( 'WP_TESTS_PHPUNIT_POLYFILLS_PATH' ) ) {
	define( 'WP_TESTS_PHPUNIT_POLYFILLS_PATH', dirname( __FILE__ ) . '/tmp/wordpress-tests-lib/vendor/yoast/phpunit-polyfills/' );
}

$_tests_dir = getenv( 'WP_TESTS_DIR' );
if ( ! $_tests_dir ) {
	$_tests_dir = '/tmp/wordpress-tests-lib';
}

require_once $_tests_dir . '/includes/functions.php';


/**
 * Manually load the plugin
 *
 * @since      2.0.0
 * @return     void
 */
function _manually_load_plugin() {
	require dirname( __FILE__ ) . '/../class-beaverlodge.php';
}
tests_add_filter( 'muplugins_loaded', '_manually_load_plugin' );

require $_tests_dir . '/includes/bootstrap.php';

echo 'Installing BeaverLodge...';

global $current_user, $beaverlodge_options;

$beaverlodge_options = get_option( 'beaverlodge_settings' );

$current_user = new WP_User( 1 ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride
$current_user->set_role( 'administrator' );
wp_update_user(
	array(
		'ID'         => 1,
		'first_name' => 'Admin',
		'last_name'  => 'User',
	)
);

/**
 * Disable HTTP requests
 *
 * @since      2.0.0
 * @param      mixed  $status Unused.
 * @param      array  $args Unused.
 * @param      string $url Unused.
 * @return     object WP_Error instance
 */
function _disable_reqs( $status = false, $args = array(), $url = '' ) {
	return new WP_Error( 'no_reqs_in_unit_tests', __( 'HTTP requests disabled for unit tests', 'beaverlodge' ) );
}
add_filter( 'pre_http_request', '_disable_reqs' );
