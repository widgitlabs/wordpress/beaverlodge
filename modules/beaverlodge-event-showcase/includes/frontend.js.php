(function($) {
    $(function() {
        window['beaverlodge_event_showcase_<?php echo $id; ?>'] = new BeaverLodge_Event_Showcase({
            id: '<?php echo $id ?>',
            perView: <?php echo esc_attr( $settings->per_view ); ?>,
        });
    });
})(jQuery);