<?php
/**
 * Hero Image module frontend CSS
 *
 * @package     BeaverLodge\Modules\Hero_Image\Frontend\CSS
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-overlay {
	<?php if ( $settings->background_color ) : ?>
		<?php if ( stristr( $settings->background_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->background_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->background_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-content-box {
	<?php if ( $settings->content_box_color ) : ?>
		<?php if ( stristr( $settings->content_box_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->content_box_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->content_box_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->content_box_text_color ) : ?>
		<?php if ( stristr( $settings->content_box_text_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->content_box_text_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->content_box_text_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->content_box_margin_bottom ) : ?>
		margin-bottom: <?php esc_attr_e( $settings->content_box_margin_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_margin_left ) : ?>
		margin-left: <?php esc_attr_e( $settings->content_box_margin_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_margin_right ) : ?>
		margin-right: <?php esc_attr_e( $settings->content_box_margin_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_margin_top ) : ?>
		margin-top: <?php esc_attr_e( $settings->content_box_margin_top ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_padding_bottom ) : ?>
		padding-bottom: <?php esc_attr_e( $settings->content_box_padding_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_padding_left ) : ?>
		padding-left: <?php esc_attr_e( $settings->content_box_padding_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_padding_right ) : ?>
		padding-right: <?php esc_attr_e( $settings->content_box_padding_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_padding_top ) : ?>
		padding-top: <?php esc_attr_e( $settings->content_box_padding_top ); ?>px;
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-heading {
	<?php if ( $settings->content_box_heading_color ) : ?>
		<?php if ( stristr( $settings->content_box_heading_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->content_box_heading_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->content_box_heading_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->content_box_heading_margin_bottom ) : ?>
		margin-bottom: <?php esc_attr_e( $settings->content_box_heading_margin_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_heading_margin_left ) : ?>
		margin-left: <?php esc_attr_e( $settings->content_box_heading_margin_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_heading_margin_right ) : ?>
		margin-right: <?php esc_attr_e( $settings->content_box_heading_margin_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_heading_margin_top ) : ?>
		margin-top: <?php esc_attr_e( $settings->content_box_heading_margin_top ); ?>px;
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-text {
	<?php if ( $settings->content_box_text_margin_bottom ) : ?>
		margin-bottom: <?php esc_attr_e( $settings->content_box_text_margin_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_text_margin_left ) : ?>
		margin-left: <?php esc_attr_e( $settings->content_box_text_margin_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_text_margin_right ) : ?>
		margin-right: <?php esc_attr_e( $settings->content_box_text_margin_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->content_box_text_margin_top ) : ?>
		margin-top: <?php esc_attr_e( $settings->content_box_text_margin_top ); ?>px;
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-button1 a {
	<?php if ( $settings->button_one_background_color ) : ?>
		<?php if ( stristr( $settings->button_one_background_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->button_one_background_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->button_one_background_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_one_color ) : ?>
		<?php if ( stristr( $settings->button_one_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->button_one_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->button_one_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_one_margin_bottom ) : ?>
		margin-bottom: <?php esc_attr_e( $settings->button_one_margin_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_margin_left ) : ?>
		margin-left: <?php esc_attr_e( $settings->button_one_margin_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_margin_right ) : ?>
		margin-right: <?php esc_attr_e( $settings->button_one_margin_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_margin_top ) : ?>
		margin-top: <?php esc_attr_e( $settings->button_one_margin_top ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_padding_bottom ) : ?>
		padding-bottom: <?php esc_attr_e( $settings->button_one_padding_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_padding_left ) : ?>
		padding-left: <?php esc_attr_e( $settings->button_one_padding_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_padding_right ) : ?>
		padding-right: <?php esc_attr_e( $settings->button_one_padding_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_padding_top ) : ?>
		padding-top: <?php esc_attr_e( $settings->button_one_padding_top ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_one_align ) : ?>
		text-align: <?php esc_attr_e( $settings->button_one_align ); ?>;
	<?php endif; ?>

	<?php if ( 'wide' === $settings->button_one_width ) : ?>
		width: 100%;
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-button1 a:hover {
	<?php if ( $settings->button_one_background_hover_color ) : ?>
		<?php if ( stristr( $settings->button_one_background_hover_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->button_one_background_hover_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->button_one_background_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_one_border_hover_color ) : ?>
		<?php if ( stristr( $settings->button_one_border_hover_color, 'rgb' ) ) : ?>
			border-color: <?php esc_attr_e( $settings->button_one_border_hover_color ); ?>;
		<?php else : ?>
			border-color: #<?php esc_attr_e( $settings->button_one_border_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_one_hover_color ) : ?>
		<?php if ( stristr( $settings->button_one_hover_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->button_one_hover_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->button_one_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-button2 a {
	<?php if ( $settings->button_two_background_color ) : ?>
		<?php if ( stristr( $settings->button_two_background_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->button_two_background_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->button_two_background_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_two_color ) : ?>
		<?php if ( stristr( $settings->button_two_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->button_two_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->button_two_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_two_margin_bottom ) : ?>
		margin-bottom: <?php esc_attr_e( $settings->button_two_margin_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_margin_left ) : ?>
		margin-left: <?php esc_attr_e( $settings->button_two_margin_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_margin_right ) : ?>
		margin-right: <?php esc_attr_e( $settings->button_two_margin_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_margin_top ) : ?>
		margin-top: <?php esc_attr_e( $settings->button_two_margin_top ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_padding_bottom ) : ?>
		padding-bottom: <?php esc_attr_e( $settings->button_two_padding_bottom ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_padding_left ) : ?>
		padding-left: <?php esc_attr_e( $settings->button_two_padding_left ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_padding_right ) : ?>
		padding-right: <?php esc_attr_e( $settings->button_two_padding_right ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_padding_top ) : ?>
		padding-top: <?php esc_attr_e( $settings->button_two_padding_top ); ?>px;
	<?php endif; ?>

	<?php if ( $settings->button_two_align ) : ?>
		text-align: <?php esc_attr_e( $settings->button_two_align ); ?>;
	<?php endif; ?>

	<?php if ( 'wide' === $settings->button_two_width ) : ?>
		width: 100%;
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-button2 a:hover {
	<?php if ( $settings->button_two_background_hover_color ) : ?>
		<?php if ( stristr( $settings->button_two_background_hover_color, 'rgb' ) ) : ?>
			background-color: <?php esc_attr_e( $settings->button_two_background_hover_color ); ?>;
		<?php else : ?>
			background-color: #<?php esc_attr_e( $settings->button_two_background_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_two_border_hover_color ) : ?>
		<?php if ( stristr( $settings->button_two_border_hover_color, 'rgb' ) ) : ?>
			border-color: <?php esc_attr_e( $settings->button_two_border_hover_color ); ?>;
		<?php else : ?>
			border-color: #<?php esc_attr_e( $settings->button_two_border_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $settings->button_two_hover_color ) : ?>
		<?php if ( stristr( $settings->button_two_hover_color, 'rgb' ) ) : ?>
			color: <?php esc_attr_e( $settings->button_two_hover_color ); ?>;
		<?php else : ?>
			color: #<?php esc_attr_e( $settings->button_two_hover_color ); ?>;
		<?php endif; ?>
	<?php endif; ?>
}

.fl-node-<?php esc_attr_e( $id ); ?> .beaverlodge-hero-image-call-to-action {
	<?php if ( 'horizontal' === $settings->cta_direction ) : ?>
		flex-direction: row;
	<?php else : ?>
		flex-direction: column;
	<?php endif; ?>
}
<?php
FLBuilderCSS::border_field_rule(
	array(
    	'settings'     => $settings,
    	'setting_name' => 'content_box_border',
    	'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-content-box',
	)
);

FLBuilderCSS::border_field_rule(
	array(
    	'settings'     => $settings,
    	'setting_name' => 'button_one_border',
    	'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-button1 a',
	)
);

FLBuilderCSS::border_field_rule(
	array(
    	'settings'     => $settings,
    	'setting_name' => 'button_two_border',
    	'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-button2 a',
	)
);

FLBuilderCSS::typography_field_rule(
	array(
		'settings'     => $settings,
		'setting_name' => 'content_box_heading_typography',
		'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-heading',
	)
);

FLBuilderCSS::typography_field_rule(
	array(
		'settings'     => $settings,
		'setting_name' => 'content_box_text_typography',
		'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-text',
	)
);

FLBuilderCSS::typography_field_rule(
	array(
		'settings'     => $settings,
		'setting_name' => 'button_one_typography',
		'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-button1 a',
	)
);

FLBuilderCSS::typography_field_rule(
	array(
		'settings'     => $settings,
		'setting_name' => 'button_two_typography',
		'selector'     => '.fl-node-' . $id . ' .beaverlodge-hero-image-button2 a',
	)
);
