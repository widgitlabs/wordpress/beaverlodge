(function($) {
    BeaverLodge_Event_Browser = function( settings ) {
        this.id        = settings.id;
        this.nodeClass = '.fl-node-' + settings.id;
        this.browser   = $( this.nodeClass ).find( '.beaverlodge-event-browser' ).first();
        this.perPage   = settings.perPage;
        this.ajaxUrl   = settings.ajaxUrl;
        this.date      = false;

        this._init();
    };

    BeaverLodge_Event_Browser.prototype = {
        nodeClass: '',
        perPage: '',

        _init: function() {
            var self = this;

            $('#search-events .filter-date').on('click', function(e) {
                e.preventDefault();

                self.date = $(this).data('date');

                $('#search-events .filter-date').each( function() {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    }
                });
                $(this).addClass('active');

                $(this).closest('form').submit();
            });

            $('#search-events input[type=text]').on('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);

                if (code == 13) {
                   e.preventDefault();
                   e.stopPropagation();

                   $(this).closest('form').submit();
                }
            });

            $('#search-events').on('submit', function(e) {
                e.preventDefault();

                var form = $('#search-events');
                var search = form.find('input[name=s]').val();
                var categories = new Array();
                var content = form.parent().next('.browser-content');
                var loader = content.find('.browser-loading:first');

                $('input[name="categories[]"]').each( function() {
                    if ($(this).is(':checked')) {
                        categories.push($(this).val());
                    }
                });

                const data = {
                    action: 'beaverlodge_event_browser_search',
                    search: search,
                    categories: categories.join(','),
                    date: self.date,
                };

                loader.css('display', 'flex');

                $.ajax({
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    url: self.ajaxUrl,
                    success: function(response) {
                        content.find('.event-wrapper').each( function() {
                            $(this).remove();
                        });

                        if ( response.results ) {
                            content.find('.browser-events:first').append(response.results);
                        }

                        loader.css('display', 'none');
                    }
                });
            });
        }
    };
})(jQuery);