<?php
/**
 * Hero Image module
 *
 * @package     BeaverLodge\Modules\Hero_Image
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Hero Image module class
 *
 * @since       1.0.0
 */
class BeaverLodge_Hero_Image extends FLBuilderModule {


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function __construct() {
		parent::__construct(
			array(
				'name'            => __( 'Hero Image', 'beaverlodge' ),
				'description'     => __( 'A hero image', 'beaverlodge' ),
				'group'           => __( 'BeaverLodge Kits', 'beaverlodge' ),
				'category'        => __( 'Content Modules', 'beaverlodge' ),
				'dir'             => BEAVERLODGE_DIR . 'modules/beaverlodge-hero-image/',
				'url'             => BEAVERLODGE_URL . 'modules/beaverlodge-hero-image/',
				'icon'            => 'icon.svg',
				'editor_export'   => true,
				'enabled'         => true,
				'partial_refresh' => true,
	  		)
		);
	}
}
