<?php
/**
 * Event Showcase module frontend
 *
 * @package     BeaverLodge\Modules\Event_Showcase\Frontend
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$event_args = array(
    'ends_after'     => 'now',
    'posts_per_page' => $settings->count,
    'order'          => $settings->order,
    'orderby'        => $settings->order_by,
    'offset'         => absint( $settings->offset ),
);

$categories = $settings->categories;

if ( 'no' === $settings->categories_match ) {
    $cat_array  = explode( ',', $categories );
    $categories = array();

    foreach ( $cat_array as $cat ) {
        $categories[] = -1 * abs( $cat );
    }

    $categories = implode( ',', $categories );
}

if ( ! empty( $categories ) ) {
    $event_args['cat'] = $categories;
}

$tags = $settings->tags;
$tags = explode( ',', $tags );

foreach ( $tags as $tag_id => $tag ) {
    if ( 'no' === $settings->tags_match ) {
        $tags[ $tag_id ] = -1 * abs( $tag );
    } else {
        $tags[ $tag_id ] = abs( $tag );
    }
}

if ( ! empty( $tags ) ) {
    if ( 'no' === $settings->tags_match ) {
        $event_args['tag__not_in'] = $tags;
    } else {
        $event_args['tag__in'] = $tags;
    }
}

if ( 'featured' === $settings->featured ) {
    $event_args['featured'] = true;
} elseif( 'not_featured' === $settings->featured ) {
    $event_args['featured'] = false;
}

$events = tribe_get_events( $event_args );
?>
<div class="beaverlodge-event-showcase swiper">
    <div class="swiper-wrapper">
        <?php foreach ( $events as $event ) : ?>
            <?php
                $link       = tribe_get_event_link( $event->ID );
                $image      = tribe_event_featured_image( $event->ID, 'full', false, false );
                $start_date = tribe_get_start_date( $event->ID, false, 'M j' );
                $end_date   = tribe_get_end_date( $event->ID, false, 'M j' );
                $date_string = $start_date;
                
                if ( $start_date !== $end_date ) {
                    $date_string .= ' - ' . $end_date;
                }
            ?>

            <?php if ( $image ) : ?>
                <div class="event-wrapper swiper-slide">
                    <a class="event-link" href="<?php echo esc_url( $link ); ?>">
                        <div class="event-image">
                            <img src="<?php echo esc_url( $image ); ?>" style="width: 100%" />
                        </div>
                        <div class="event-header">
                            <div class="event-date"><?php echo esc_html( $date_string ); ?></div>
                            <div class="event-title"><?php echo $event->post_title; ?></div>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
</div>
