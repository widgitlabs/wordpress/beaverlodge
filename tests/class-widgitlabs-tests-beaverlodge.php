<?php
/**
 * Core unit tests
 *
 * @package     Widgit\Mod_Manager\Tests\Demo
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Core unit tests
 *
 * @since       1.0.0
 */
class BeaverLodge_Instance extends WP_UnitTestCase {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions

	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::setUp();
	}

	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}

	/**
	 * Test instance
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 * @covers      BeaverLodger::instance
	 * @covers      BeaverLodge::setup_constants
	 * @uses        ::beaverlodge
	 */
	public function test_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'BeaverLodge' );
		$this->assertIsCallable( 'beaverlodge' );

		$plugin_url = trailingslashit( str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) ) );
		$this->assertSame( BEAVERLODGE_URL, $plugin_url );

		$plugin_path  = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$plugin_path  = trailingslashit( substr( $plugin_path, 0, -1 ) );
		$compare_path = trailingslashit( substr( BEAVERLODGE_DIR, 0, -1 ) );
		$this->assertSame( $plugin_path, $compare_path );

		$this->assertFileExists( $plugin_path . 'bin/docker_install.sh' );
		$this->assertFileExists( $plugin_path . 'bin/install-wp-tests.sh' );
		$this->assertFileExists( $plugin_path . 'languages/README.md' );
		$this->assertFileExists( $plugin_path . 'tests/bootstrap.php' );
		$this->assertFileExists( $plugin_path . 'tests/class-widgitlabs-tests-beaverlodge.php' );
		$this->assertFileExists( $plugin_path . '.changeloglint.json' );
		$this->assertFileExists( $plugin_path . '.editorconfig' );
		$this->assertFileExists( $plugin_path . '.eslintrc.json' );
		$this->assertFileExists( $plugin_path . '.gitattributes' );
		$this->assertFileExists( $plugin_path . '.gitignore' );
		$this->assertFileExists( $plugin_path . '.markdownlint.json' );
		$this->assertFileExists( $plugin_path . '.markdownlintignore' );
		$this->assertFileExists( $plugin_path . '.stylelintignore' );
		$this->assertFileExists( $plugin_path . '.stylelintrc' );
		$this->assertFileExists( $plugin_path . 'CHANGELOG.md' );
		$this->assertFileExists( $plugin_path . 'class-beaverlodge.php' );
		$this->assertFileExists( $plugin_path . 'composer.json' );
		$this->assertFileExists( $plugin_path . 'CONTRIBUTING.md' );
		$this->assertFileExists( $plugin_path . 'Gruntfile.js' );
		$this->assertFileExists( $plugin_path . 'license.txt' );
		$this->assertFileExists( $plugin_path . 'package-lock.json' );
		$this->assertFileExists( $plugin_path . 'package.json' );
		$this->assertFileExists( $plugin_path . 'phpcs.xml' );
		$this->assertFileExists( $plugin_path . 'phpunit.xml' );
		$this->assertFileExists( $plugin_path . 'README.md' );
		$this->assertFileExists( $plugin_path . 'readme.txt' );
	}
}
