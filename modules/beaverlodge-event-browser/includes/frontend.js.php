(function($) {
    $(function() {
        window['beaverlodge_event_browser_<?php echo $id; ?>'] = new BeaverLodge_Event_Browser({
            ajaxUrl: '<?php echo beaverlodge_get_ajax_url(); ?>',
            id: '<?php echo $id ?>',
            perPage: <?php echo esc_attr( $settings->per_page ); ?>
        });
    });
})(jQuery);