<?php
/**
 * Helper functions
 *
 * @package     BeaverLodge\Functions
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Get the ajax URL.
 * 
 * @since       1.0.0
 * @return      string $ajax_url The ajax URL.
 */
function beaverlodge_get_ajax_url() {
    $scheme = defined( 'FORCE_SSL_ADMIN' ) && FORCE_SSL_ADMIN ? 'https' : 'admin';

    $current_url = beaverlodge_get_current_page_url();
    $ajax_url    = admin_url( 'admin-ajax.php', $scheme );

    if ( preg_match( '/^https/', $current_url ) && ! preg_match( '/^https/', $ajax_url ) ) {
		$ajax_url = preg_replace( '/^http/', 'https', $ajax_url );
	}

    return apply_filters( 'beaverlodge_ajax_url', $ajax_url );
}


/**
 * Get the current page URL
 *
 * @since       1.0.0
 * @param       bool $nocache  If we should bust cache on the returned URL.
 * @return      string $page_url Current page URL.
 */
function beaverlodge_get_current_page_url( $nocache = false ) {
	global $wp;

	if ( get_option( 'permalink_structure' ) ) {
		$base = trailingslashit( home_url( $wp->request ) );
	} else {
		$base = add_query_arg( $wp->query_string, '', trailingslashit( home_url( $wp->request ) ) );
		$base = remove_query_arg( array( 'post_type', 'name' ), $base );
	}

	$scheme = is_ssl() ? 'https' : 'http';
	$uri    = set_url_scheme( $base, $scheme );

	if ( is_front_page() ) {
		$uri = home_url( '/' );
	}

	$uri = apply_filters( 'beaverlodge_get_current_page_url', $uri );

	if ( $nocache ) {
		$uri = beaverlodge_add_cache_busting( $uri );
	}

	return $uri;
}


/**
 * Adds the 'nocache' parameter to the provided URL
 *
 * @since       1.0.0
 * @param       string $url The URL being requested.
 * @return      string $url The URL with cache busting added or not.
 */
function beaverlodge_add_cache_busting( $url = '' ) {
	if ( beaverlodge_is_caching_plugin_active() ) {
		$url = add_query_arg( 'nocache', 'true', $url );
	}

	return $url;
}


/**
 * Checks if a caching plugin is active
 *
 * @since       1.0.0
 * @return      bool $caching True if caching plugin is enabled, false otherwise.
 */
function beaverlodge_is_caching_plugin_active() {
	$caching = ( function_exists( 'wpsupercache_site_admin' ) || defined( 'W3TC' ) || function_exists( 'rocket_init' ) ) || defined( 'WPHB_VERSION' );
	
    return apply_filters( 'beaverlodge_is_caching_plugin_active', $caching );
}


/**
 * Get the markup for an individual event browser event
 * 
 * @since       1.0.0
 * @param       object $event The WordPress post object.
 * @return      string $the_event The markup for the event.
 */
function beaverlodge_event_browser_render_event( $event ) {
    $link        = tribe_get_event_link( $event->ID );
    $image       = tribe_event_featured_image( $event->ID, 'full', false, false );
    $start_date  = tribe_get_start_date( $event->ID, false, 'M j' );
    $end_date    = tribe_get_end_date( $event->ID, false, 'M j' );
    $date_string = $start_date;

    ob_start();

    if ( $image ) : ?>
        <div class="event-wrapper">
            <a class="event-link" href="<?php echo esc_url( $link ); ?>">
                <div class="event-image">
                    <img src="<?php echo esc_url( $image ); ?>" style="width: 100%" />
                </div>
                <div class="event-date"><span><?php echo str_replace( ' ', '<br />', $date_string ); ?></span></div>
                <div class="event-content">
                    <h3 class="event-title"><?php echo $event->post_title; ?></h3>
                    <div>
                    <span class="dashicons dashicons-location"></span>
                        <?php echo tribe_get_venue( $event->ID ); ?>
                    </div>
                </div>
            </a>
        </div>
    <?php endif;

    $event = ob_get_clean();

    return $event;
}


function beaverlodge_event_browser_get_search_results() {
    $get = wp_parse_args( $_GET );

    $event_args = array(
        'posts_per_page' => 12,
        'order'          => 'asc',
        'orderby'        => 'date',
        'offset'         => 0,
    );

    if ( array_key_exists( 'posts_per_page', $get ) ) {
        $event_args['posts_per_page'] = absint( $get['posts_per_page'] );
    }
    
    if ( array_key_exists( 'order', $get ) ) {
        $event_args['order'] = $get['order'];
    }

    if ( array_key_exists( 'orderby', $get ) ) {
        $event_args['orderby'] = $get['orderby'];
    }

    if ( array_key_exists( 'offset', $get ) ) {
        $event_args['offset'] = absint( $get['offset'] );
    }

    if ( array_key_exists( 'categories', $get ) ) {
        if ( is_array( $get['categories'] ) ) {
            $cats = implode( ',', $get['categories'] );
        } else {
            $cats = $get['categories'];
        }
    
        $event_args['category'] = $cats;
    }

    if ( array_key_exists( 'search', $get ) && ! empty( $get['search'] ) ) {
        $event_args['search'] = $get['search'];
    }
    
    if ( array_key_exists( 'date', $get ) ) {
        switch( $get['date'] ) {
            case 'today':
                $event_args['start_date'] = $get['date'];
                $event_args['end_date'] = $get['date'];
                break;
            case 'week':
                $event_args['start_date'] = 'today';
                $event_args['end_date'] = '+1 week';
                break;
            case '':
                break;
            default:
                $event_args['start_date'] = $get['date'];
    
                if ( ! array_key_exists( 'end_date', $get ) ) {
                    $event_args['end_date'] = $get['date'];
                } else {
                    $event_args['end_date'] = $get['end_date'];
                }
                break;
        }
    }

    if ( ! array_key_exists( 'start_date', $event_args ) ) {
        $event_args['start_date'] = 'today';
    }

    $events = tribe_get_events( $event_args );

    return $events;
}


function beaverlodge_event_browser_search() {
    $events = beaverlodge_event_browser_get_search_results();
    $the_events = '';

    foreach ( $events as $event ) {
        $the_events .= beaverlodge_event_browser_render_event( $event );
    }

    echo json_encode( array( 'results' => $the_events ) );
    beaverlodge_die();
}
add_action( 'wp_ajax_beaverlodge_event_browser_search', 'beaverlodge_event_browser_search' );
add_action( 'wp_ajax_nopriv_beaverlodge_event_browser_search', 'beaverlodge_event_browser_search' );


/**
 * Wrapper function for wp_die().
 *
 * This function adds filters for wp_die() which kills execution of the script
 * using wp_die(). This allows us to then to work with functions using
 * beaverlodge_die() in the unit tests.
 *
 * @since       1.0.0
 * @param       string $message The error message.
 * @param       string $title The error title.
 * @param       int $status The error code.
 * @return      void
 */
function beaverlodge_die( $message = '', $title = '', $status = 400 ) {
	if ( ! defined( 'BEAVERLODGE_UNIT_TESTS' ) ) {
		add_filter( 'wp_die_ajax_handler', '_beaverlodge_die_handler', 10, 3 );
		add_filter( 'wp_die_handler'     , '_beaverlodge_die_handler', 10, 3 );
		add_filter( 'wp_die_json_handler', '_beaverlodge_die_handler', 10, 3 );
	}

	wp_die( $message, $title, array( 'response' => $status ) );
}


/**
 * Register die handler for beaverlodge_die()
 *
 * @since       1.0.0
 * @return      void
 */
function _beaverlodge_die_handler() {
	die();
}