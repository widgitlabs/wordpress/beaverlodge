<?php
/**
 * Hero Image module frontend
 *
 * @package     BeaverLodge\Modules\Hero_Image\Frontend
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="beaverlodge-hero-image">
	<div class="beaverlodge-hero-image-bg">
		<?php if ( $settings->background_image_src ) : ?>
			<div class="beaverlodge-hero-image-overlay"></div>
			<img src="<?php echo esc_url( $settings->background_image_src ); ?>" class="beaverlodge-hero-image-bg-image" />
		<?php endif; ?>
	</div>

	<div class="beaverlodge-hero-image-content-box">
		<?php if ( $settings->heading ) : ?>
			<<?php esc_html_e( $settings->heading_tag ); ?> class="beaverlodge-hero-image-heading"><?php esc_html_e( $settings->heading ); ?></<?php esc_html_e( $settings->heading_tag ); ?>>
		<?php endif; ?>

		<?php if ( $settings->text ) : ?>
			<div class="beaverlodge-hero-image-text"><?php esc_html_e( $settings->text ); ?></div>
		<?php endif; ?>

		<?php if ( 'none' !== $settings->cta_type ) : ?>
			<div class="beaverlodge-hero-image-call-to-action">
				<div class="beaverlodge-hero-image-button1">
					<a class="beaverlodge-hero-image-button" href="<?php esc_attr_e( $settings->button_one_link ); ?>">
						<?php if ( $settings->button_one_icon ) : ?>
							<i class="<?php esc_attr_e( $settings->button_one_icon ); ?>" />
						<?php endif; ?>

						<?php esc_html_e( $settings->button_one_text ); ?>
					</a>
				</div>

				<?php if ( 'two_buttons' === $settings->cta_type ) : ?>
					<div class="beaverlodge-hero-image-button2">
						<a class="beaverlodge-hero-image-button" href="<?php esc_attr_e( $settings->button_two_link ); ?>">
							<?php if ( $settings->button_two_icon ) : ?>
								<i class="<?php esc_attr_e( $settings->button_two_icon ); ?>" />
							<?php endif; ?>

							<?php esc_html_e( $settings->button_two_text ); ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
