<?php
/**
 * Event Showcase module
 *
 * @package     BeaverLodge\Modules\Event_Showcase
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Event Showcase module class
 *
 * @since       1.0.0
 */
class BeaverLodge_Event_Showcase extends FLBuilderModule {


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function __construct() {
		parent::__construct(
			array(
				'name'            => __( 'Event Showcase', 'beaverlodge' ),
				'description'     => __( 'Showcase your Event Calendar events', 'beaverlodge' ),
				'group'           => __( 'BeaverLodge Kits', 'beaverlodge' ),
				'category'        => __( 'Event Calendar Modules', 'beaverlodge' ),
				'dir'             => BEAVERLODGE_DIR . 'modules/beaverlodge-event-showcase/',
				'url'             => BEAVERLODGE_URL . 'modules/beaverlodge-event-showcase/',
				'icon'            => 'icon.svg',
				'editor_export'   => true,
				'enabled'         => true,
				'partial_refresh' => true,
	  		)
		);
	}
}
