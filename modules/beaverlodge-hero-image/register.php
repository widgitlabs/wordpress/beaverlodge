<?php
/**
 * Hero Image module registration
 *
 * @package     BeaverLodge\Modules\Hero_Image\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Register the hero image module
 *
 * @since       1.0.0
 * @return      void
 */
function beaverlodge_hero_image_register() {
	$settings = apply_filters( 'beaverlodge_hero_image_settings', array() );

	FLBuilder::register_module( 'BeaverLodge_Hero_Image', $settings );
}
add_action( 'init', 'beaverlodge_hero_image_register' );


/**
 * Register hero image module settings
 *
 * @since       1.0.0
 * @param       array $settings The module settings.
 * @return      array $settings The module settings.
 */
function beaverlodge_hero_image_settings( $settings ) {
	$settings = array(
		'general'  => array(
			'title'    => __( 'General', 'beaverlodge' ),
			'sections' => array(
				'core'           => array(
					'fields' => array(
						'heading'     => array(
							'type'    => 'text',
							'label'   => __( 'Heading', 'beaverlodge' ),
							'default' => __( 'Hero Image', 'beaverlodge' ),
						),
						'heading_tag' => array(
							'type'    => 'select',
							'label'   => __( 'Heading Tag', 'beaverlodge' ),
							'default' => 'h1',
							'options' => array(
								'h1'  => __( 'h1', 'beaverlodge' ),
								'h2'  => __( 'h2', 'beaverlodge' ),
								'h3'  => __( 'h3', 'beaverlodge' ),
								'h4'  => __( 'h4', 'beaverlodge' ),
								'h5'  => __( 'h5', 'beaverlodge' ),
								'h6'  => __( 'h6', 'beaverlodge' ),
								'div' => __( 'div', 'beaverlodge' ),
							),
						),
					),
				),
				'text'           => array(
					'title'  => __( 'Text', 'beaverlodge' ),
					'fields' => array(
						'text' => array(
							'type'    => 'editor',
							'wpautop' => false,
						),
					),
				),
				'call_to_action' => array(
					'title'  => __( 'Call to Action', 'beaverlodge' ),
					'fields' => array(
						'cta_type'      => array(
							'type'    => 'select',
							'label'   => __( 'Type', 'beaverlodge' ),
							'default' => 'none',
							'options' => array(
								'none'        => __( 'None', 'beaverlodge' ),
								'one_button'  => __( 'One Button', 'beaverlodge' ),
								'two_buttons' => __( 'Two Buttons', 'beaverlodge' ),
							),
							'toggle'  => array(
								'one_button'  => array(
									'tabs' => array( 'button_one' ),
								),
								'two_buttons' => array(
									'tabs'   => array(
										'button_one',
										'button_two'
									),
									'fields' => array( 'cta_direction' ),
								),
							),
						),
						'cta_direction' => array(
							'type'    => 'select',
							'label'   => __( 'Direction', 'beaverlodge' ),
							'default' => 'horizontal',
							'options' => array(
								'horizontal' => __( 'Horizontal', 'beaverlodge' ),
								'vertical'   => __( 'Vertical', 'beaverlodge' ),
							),
						),
					),
				),
			),
		),
		'button_one' => array(
			'title'  => __( 'Button One', 'beaverlodge' ),
			'sections' => array(
				'button_one_core'       => array(
					'fields' => array(
						'button_one_text' => array(
							'type'    => 'text',
							'label'   => __( 'Text', 'beaverlodge' ),
							'default' => __( 'Button One', 'beaverlodge' ),
						),
						'button_one_link' => array(
							'type'          => 'link',
							'label'         => 'Link',
							'show_target'   => true,
							'show_nofollow' => true,
							'default'       => '#',
						),
					),
				),
				'button_one_icon'       => array(
					'title'  => __( 'Button Icon', 'beaverlodge' ),
					'fields' => array(
						'button_one_icon' => array(
							'type'        => 'icon',
							'label'       => __( 'Icon', 'beaverlodge' ),
							'show_remove' => true,
						),
					),
				),
				'button_one_style'      => array(
					'title'  => __( 'Button Style', 'beaverlodge' ),
					'fields' => array(
						'button_one_width' => array(
							'type'    => 'select',
							'label'   => __( 'Button Width', 'beaverlodge' ),
							'default' => 'auto',
							'options' => array(
								'auto' => __( 'Auto', 'beaverlodge' ),
								'full' => __( 'Full Width', 'beaverlodge' ),
							),
						),
						'button_one_align' => array(
							'type'       => 'align',
							'label'      => __( 'Button Align', 'beaverlodge' ),
							'default'    => 'left',
							'responsive' => true,
						),
						'button_one_margin' => array(
							'type'        => 'dimension',
							'label'       => __( 'Button Margin', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
						'button_one_padding' => array(
							'type'        => 'dimension',
							'label'       => __( 'Button Padding', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
					),
				),
				'button_one_text'       => array(
					'title'  => __( 'Button Text', 'beaverlodge' ),
					'fields' => array(
						'button_one_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Text Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_one_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Text Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_one_typography' => array(
							'type'       => 'typography',
							'label'      => __( 'Button Typography', 'beaverlodge' ),
							'responsive' => true,
						),
					),
				),
				'button_one_background' => array(
					'title'  => __( 'Button Background', 'beaverlodge' ),
					'fields' => array(
						'button_one_background_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Background Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_one_background_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Background Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
					),
				),
				'button_one_border'     => array(
					'title'  => __( 'Button Border', 'beaverlodge' ),
					'fields' => array(
						'button_one_border' => array(
							'type'       => 'border',
							'label'      => __( 'Button Border', 'beaverlodge' ),
							'responsive' => true,
						),
						'button_one_border_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Border Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
					),
				),
			),
		),
		'button_two' => array(
			'title'  => __( 'Button Two', 'beaverlodge' ),
			'sections' => array(
				'button_two_core'       => array(
					'fields' => array(
						'button_two_text' => array(
							'type'    => 'text',
							'label'   => __( 'Text', 'beaverlodge' ),
							'default' => __( 'Button Two', 'beaverlodge' ),
						),
						'button_two_link' => array(
							'type'          => 'link',
							'label'         => 'Link',
							'show_target'   => true,
							'show_nofollow' => true,
							'default'       => '#',
						),
					),
				),
				'button_two_icon'       => array(
					'title'  => __( 'Button Icon', 'beaverlodge' ),
					'fields' => array(
						'button_two_icon' => array(
							'type'        => 'icon',
							'label'       => __( 'Icon', 'beaverlodge' ),
							'show_remove' => true,
						),
					),
				),
				'button_two_style'      => array(
					'title'  => __( 'Button Style', 'beaverlodge' ),
					'fields' => array(
						'button_two_width' => array(
							'type'    => 'select',
							'label'   => __( 'Button Width', 'beaverlodge' ),
							'default' => 'auto',
							'options' => array(
								'auto' => __( 'Auto', 'beaverlodge' ),
								'full' => __( 'Full Width', 'beaverlodge' ),
							),
						),
						'button_two_align' => array(
							'type'       => 'align',
							'label'      => __( 'Button Align', 'beaverlodge' ),
							'default'    => 'left',
							'responsive' => true,
						),
						'button_two_margin' => array(
							'type'        => 'dimension',
							'label'       => __( 'Button Margin', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
						'button_two_padding' => array(
							'type'        => 'dimension',
							'label'       => __( 'Button Padding', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
					),
				),
				'button_two_text'       => array(
					'title'  => __( 'Button Text', 'beaverlodge' ),
					'fields' => array(
						'button_two_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Text Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_two_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Text Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_two_typography' => array(
							'type'       => 'typography',
							'label'      => __( 'Button Typography', 'beaverlodge' ),
							'responsive' => true,
						),
					),
				),
				'button_two_background' => array(
					'title'  => __( 'Button Background', 'beaverlodge' ),
					'fields' => array(
						'button_two_background_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Background Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'button_two_background_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Background Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
					),
				),
				'button_two_border'     => array(
					'title'  => __( 'Button Border', 'beaverlodge' ),
					'fields' => array(
						'button_two_border' => array(
							'type'       => 'border',
							'label'      => __( 'Button Border', 'beaverlodge' ),
							'responsive' => true,
						),
						'button_two_border_hover_color' => array(
							'type'       => 'color',
							'label'      => __( 'Button Border Hover Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
					),
				),
			),
		),
		'style' => array(
			'title'  => __( 'Style', 'beaverlodge' ),
			'sections' => array(
				'background' => array(
					'title'  => __( 'Background Image', 'beaverlodge' ),
					'fields' => array(
						'background_image' => array(
							'type'        => 'photo',
							'label'       => __( 'Background Image', 'beaverlodge' ),
							'show_remove' => false,
						),
						'background_color' => array(
							'type'       => 'color',
							'label'      => __( 'Background Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
							'default'    => '#fff',
						),
					),
				),
				'content_box'      => array(
					'title'  => __( 'Content Box', 'beaverlodge' ),
					'fields' => array(
						'content_box_color' => array(
							'type'       => 'color',
							'label'      => __( 'Content Box Background Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'content_box_border' => array(
							'type'       => 'border',
							'label'      => __( 'Content Box Border', 'beaverlodge' ),
							'responsive' => true,
						),
						'content_box_margin' => array(
							'type'        => 'dimension',
							'label'       => __( 'Content Box Margin', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
						'content_box_padding' => array(
							'type'        => 'dimension',
							'label'       => __( 'Content Box Padding', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
					),
				),
				'content_box_heading'          => array(
					'title'  => __( 'Heading', 'beaverlodge' ),
					'fields' => array(
						'content_box_heading_color' => array(
							'type'       => 'color',
							'label'      => __( 'Heading Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'content_box_heading_typography' => array(
							'type'       => 'typography',
							'label'      => __( 'Heading Typography', 'beaverlodge' ),
							'responsive' => true,
						),
						'content_box_heading_margin' => array(
							'type'        => 'dimension',
							'label'       => __( 'Heading Margin', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
					),
				),
				'content_box_text'             => array(
					'title'  => __( 'Text', 'beaverlodge' ),
					'fields' => array(
						'content_box_text_color' => array(
							'type'       => 'color',
							'label'      => __( 'Text Color', 'beaverlodge' ),
							'show_reset' => true,
							'show_alpha' => true,
						),
						'content_box_text_typography' => array(
							'type'       => 'typography',
							'label'      => __( 'Text Typography', 'beaverlodge' ),
							'responsive' => true,
						),
						'content_box_text_margin' => array(
							'type'        => 'dimension',
							'label'       => __( 'Text Margin', 'beaverlodge' ),
							'description' => 'px',
							'responsive'  => true,
						),
					),
				),
			),
		),
	);

	return $settings;
}
add_filter( 'beaverlodge_hero_image_settings', 'beaverlodge_hero_image_settings' );
