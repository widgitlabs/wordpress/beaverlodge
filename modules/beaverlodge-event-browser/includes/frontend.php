<?php
/**
 * Event Browser module frontend
 *
 * @package     BeaverLodge\Modules\Event_Browser\Frontend
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$terms          = get_terms( 'tribe_events_cat' );
$get            = wp_parse_args( $_GET );
$events         = beaverlodge_event_browser_get_search_results();
$posts_per_page = $settings->per_page ? absint( $settings->per_page ) : 12;
$order          = $settings->order ? $settings->order : 'asc';
$orderby        = $settings->orderby ? $settings->orderby : 'date';
$offset         = $settings->offset ? absint( $settings->offset ) : 0;
$date           = ( array_key_exists( 'date', $get ) && ! empty( $get['date'] ) ) ? $get['date'] : false;
?>
<div class="beaverlodge-event-browser">
    <div class="browser-search">
        <form id="search-events">
            <div>
                <?php $search = array_key_exists( 'search', $get ) ? $get['search'] : ''; ?>
                <input type="text" name="s" value="<?php echo esc_attr( $search ); ?>" placeholder="<?php esc_attr_e( 'Search', 'beaverlodge' ); ?>" />
            </div>
            
            <h4><?php esc_html_e( 'Filter by Date', 'beaverlodge' ); ?></h4>
            <div class="search-by-date">
                <?php $active = ! $date ? ' active' : ''; ?>
                <a href="<?php echo add_query_arg( 'date', false ); ?>" class="filter-date<?php echo esc_attr( $active ); ?>" data-date=""><?php esc_html_e( 'Any', 'beaverlodge' ); ?></a>

                <?php $active = ( $date && 'today' === $date ) ? ' active' : ''; ?>
                <a href="<?php echo add_query_arg( 'date', 'today' ); ?>" class="filter-date<?php echo esc_attr( $active ); ?>" data-date="today"><?php esc_html_e( 'Today', 'beaverlodge' ); ?></a>
            </div>

            <h4><?php esc_html_e( 'Categories', 'beaverlodge' ); ?></h4>
            <?php foreach ( $terms as $term ) : ?>
                <?php
                    $checked = array_key_exists( 'categories', $get ) && in_array( esc_attr( absint( $term->term_id ) ), $get['categories'] );
                    $checked = $checked ? 'checked' : '';
                ?>
                <div>
                    <label>
                        <input type="checkbox" name="categories[]" value="<?php echo esc_attr( absint( $term->term_id ) ); ?>" <?php echo $checked; ?> />
                        <?php echo esc_html( $term->name ); ?>
                    </label>
                </div>
            <?php endforeach; ?>
            
            <div>
                <input type="hidden" name="posts_per_page" value="<?php echo esc_attr( $posts_per_page ); ?>" />
                <input type="hidden" name="order" value="<?php echo esc_attr( $order ); ?>" />
                <input type="hidden" name="orderby" value="<?php echo esc_attr( $orderby ); ?>" />
                <input type="hidden" name="offset" value="<?php echo esc_attr( $offset ); ?>" />

                <input type="submit" value="<?php esc_html_e( 'Search', 'beaverlodge' ); ?>" />
            </div>
        </form>
    </div>
    <div class="browser-content">
        <div class="browser-loading"><i class="fa-solid fa-spinner fa-spin-pulse"></i></div>
        <div class="browser-settings"></div>
        <div class="browser-events">
            <?php
            foreach ( $events as $event ) {
                $the_event = beaverlodge_event_browser_render_event( $event );

                echo $the_event;
            }
            ?>
        </div>
    </div>
</div>
